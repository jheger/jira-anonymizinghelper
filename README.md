README
=

## REST API

### GET /rest/anonhelper/latest/applicationuser

Returns the data from the Jira-class `ApplicationUser` for the user
given in the URL-parameter `username` or `key`.

#### Query parameters

- username: The user-name, or
- key: The user-key.

These parameters can be given muliple times and they can be mixed.

#### Permission

Need Admin permissions.

#### Responses

- 200 OK: Returns a representation of the user as Jira-class `ApplicationUser`
- 401 Unauthorized: Returned if you are not a Jira-user.
- 403 Forbidden: Returned if you do not have Administrator permissions.

#### Example-response in case of 200 OK

    {
        "user": {
            "key": "JIRAUSER10103",
            "id": 10103,
            "name": "user1",
            "displayName": "User-1",
            "username": "user1",
            "active": true,
            "directoryId": 1,
            "directoryUser": {
                "name": "user1",
                "displayName": "User-1",
                "active": true,
                "directoryId": 1,
                "emailAddress": "user1@example.com"
            },
            "emailAddress": "user1@example.com"
        }
    }

#### Example calls

o Get user 'admin' by user-name `admin`:

`GET /rest/anonhelper/latest/applicationuser?username=admin`

o Get user 'admin' by user-key `JIRAUSER10100`: 

`GET /rest/anonhelper/latest/applicationuser?key=JIRAUSER10100`

o Get two users by their user-names `admin` and `user1`:

`GET /rest/anonhelper/latest/applicationuser?username=admin&username=user1`

o Get both users by their user-keys `JIRAUSER10100` and `JIIRAUSER10103`:

`GET /rest/anonhelper/latest/applicationuser?key=JIRAUSER10100&key=JIRAUSER10103`

o Mixed, get 'admin' by user-name and 'user1' by user-key:

`GET /rest/anonhelper/latest/applicationuser?username=admin&key=JIRAUSER10103`

#### Check

`$ curl --insecure -u admin:admin http://localhost:2990/jira/rest/anonhelper/latest/applicationuser?username=admin`

## Download

See the `jar` directory.
