package jh.atlassian.jira.anonymizinghelper;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class RestService {
    private static final Logger log = LoggerFactory.getLogger(RestService.class);
    private final GlobalPermissionManager globalPermissionManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserManager userManager;

    public RestService(GlobalPermissionManager globalPermissionManager, JiraAuthenticationContext jiraAuthenticationContext,
                       UserManager userManager) {
        this.globalPermissionManager = globalPermissionManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userManager = userManager;
    }


    @GET
    @Path("/applicationuser")
    public Response getApplicationUser(@QueryParam("username") final List<String> usernames,
                                       @QueryParam("key") final List<String> keys,
                                       @Context HttpServletRequest requestContext) throws IOException {

        if (!jiraAuthenticationContext.isLoggedInUser()) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, jiraAuthenticationContext.getLoggedInUser())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        Map<String, Object> entities = new HashMap<>();
        if (!usernames.isEmpty()) {
            usernames.stream().forEach(u -> {
                ApplicationUser applicationUser = userManager.getUserByName(u);
                Map<String, Object> m = new LinkedHashMap<>();
                if (applicationUser != null) {
                    m.put("appUserId", applicationUser.getId());
                    m.put("anonymizedDisplayName", anonymizedDisplayName(applicationUser));
                    entities.put(u, m);
                } else {
                    entities.put(u, Collections.EMPTY_MAP);
                }
            });
        }
        if (!keys.isEmpty()) {
            keys.stream().forEach(k -> {
                ApplicationUser applicationUser = userManager.getUserByKey(k);
                Map<String, Object> m = new LinkedHashMap<>();
                if (applicationUser != null) {
                    m.put("appUserId", applicationUser.getId());
                    m.put("anonymizedDisplayName", anonymizedDisplayName(applicationUser));
                    entities.put(k, m);
                } else {
                    entities.put(k, Collections.EMPTY_MAP);
                }
            });
        }

        ObjectMapper om = new ObjectMapper();
        return Response.status(Response.Status.OK).entity(om.writeValueAsString(entities)).build();
    }

    private String anonymizedDisplayName(ApplicationUser user) {
        final byte[] idBytes = ByteBuffer.allocate(Long.BYTES).putLong(user.getId()).array();
        final String idHash = sha256Hex(idBytes);
        return "user-" + idHash.substring(0, 5);
    }


}
